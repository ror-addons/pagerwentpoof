local version = "0.3.3"
if not PagerWentPoof then PagerWentPoof = {} end
local pairs = pairs

function PagerWentPoof.Initialize()
	-- loading SavedVariables
	if PagerWentPoof.Settings == nil then
		PagerWentPoof.Settings = {hiding = true}
	end

	LibSlash.RegisterSlashCmd("pwp", function(msg) PagerWentPoof.Set(msg) end)

	if PagerWentPoof.Settings.hiding then
		PagerWentPoof.HideThem()
	end
end

function PagerWentPoof.OnShutdown()
	-- try to revert changes on every shutdown so that addon can be just disabled/deleted
	PagerWentPoof.UnhideThem()
end

function PagerWentPoof.HideThem()
	PagerWentPoof.Settings.hiding = true
	ActionBarConstants.SHOW_PAGE_SELECTOR_RIGHT = 44 --43 originally
	ActionBarConstants.SHOW_PAGE_SELECTOR_LEFT = 44 --42 originally
	
	for k1,v1 in pairs(ActionBarClusterSettings) do
		if k1 ~= "layoutMode" then
			v1["selector"] = 44
		end
	end
	ActionBarClusterManager.ReanchorCluster()
end

function PagerWentPoof.UnhideThem()
	--[[
		I encountered a bug when it unhid pagers on stance and granted abilities bars and
		the bars became messed up by normal abilities.
		This can be fixed by setting amount of buttons of any action bar to a big amount (like 116)
		Button amount is set in .../EASystem_ActionBarClusterManager/SavedVariables.lua
		Reverting back can be done from inside the game using bar settings.
	--]]
	PagerWentPoof.Settings.hiding = false
	ActionBarConstants.SHOW_PAGE_SELECTOR_RIGHT = 43 --43 originally
	ActionBarConstants.SHOW_PAGE_SELECTOR_LEFT = 42 --42 originally
	for k1,v1 in pairs(ActionBarClusterSettings) do
		if k1 ~= "layoutMode" then
			if v1["barId"] >= 1 and v1["barId"] <= 6 then -- do only for normal bars (not granted/stance bars)
				v1["selector"] = 43
			end
		end
	end
	ActionBarClusterManager.ReanchorCluster()
end

function PagerWentPoof.Set(msg)
	if msg == "1" then
		PagerWentPoof.HideThem()
		TextLogAddEntry("Chat", 0, L"PagerWentPoof - hiding pagers. Please /reloadui to apply changes.")
	else
		PagerWentPoof.UnhideThem()
		TextLogAddEntry("Chat", 0, L"PagerWentPoof - unhiding pagers. Please /reloadui to apply changes.")
	end
end