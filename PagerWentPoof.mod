<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="PagerWentPoof" version="0.3.3" date="5/07/2017">
		<Author name="Computerpunk" email="teromario@yahoo.com" />
		<Description text="Pager Hider. Use '/pwp 1' to hide them, and '/pwp' to unhide. Modded by anon" />
		<VersionSettings gameVersion="1.4.1" windowsVersion="1.0" savedVariablesVersion="1.1" />
		<Dependencies>
			<Dependency name="EASystem_ActionBarClusterManager" />
			<Dependency name="LibSlash" />
		</Dependencies>
		<Files>
			<File name="PagerWentPoof.lua" />
		</Files>
		<OnInitialize>
			<CallFunction name="PagerWentPoof.Initialize" />
		</OnInitialize>
		<OnShutdown>
			<CallFunction name="PagerWentPoof.OnShutdown" />
		</OnShutdown>
		<SavedVariables>
            <SavedVariable name="PagerWentPoof.Settings" />
        </SavedVariables>
		<WARInfo>
	<Categories>
		<Category name="ACTION_BARS" />
		<Category name="SYSTEM" />
	</Categories>
	<Careers>
		<Career name="BLACKGUARD" />
		<Career name="WITCH_ELF" />
		<Career name="DISCIPLE" />
		<Career name="SORCERER" />
		<Career name="IRON_BREAKER" />
		<Career name="SLAYER" />
		<Career name="RUNE_PRIEST" />
		<Career name="ENGINEER" />
		<Career name="BLACK_ORC" />
		<Career name="CHOPPA" />
		<Career name="SHAMAN" />
		<Career name="SQUIG_HERDER" />
		<Career name="WITCH_HUNTER" />
		<Career name="KNIGHT" />
		<Career name="BRIGHT_WIZARD" />
		<Career name="WARRIOR_PRIEST" />
		<Career name="CHOSEN" />
		<Career name= "MARAUDER" />
		<Career name="ZEALOT" />
		<Career name="MAGUS" />
		<Career name="SWORDMASTER" />
		<Career name="SHADOW_WARRIOR" />
		<Career name="WHITE_LION" />
		<Career name="ARCHMAGE" />
	</Careers>
		</WARInfo>
	</UiMod>
</ModuleFile>
